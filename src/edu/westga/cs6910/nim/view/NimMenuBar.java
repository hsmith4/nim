package edu.westga.cs6910.nim.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.strategy.CautiousStrategy;
import edu.westga.cs6910.nim.model.strategy.ExpertStrategy;
import edu.westga.cs6910.nim.model.strategy.GreedyStrategy;
import edu.westga.cs6910.nim.model.strategy.NumberOfSticksStrategy;
import edu.westga.cs6910.nim.model.strategy.RandomStrategy;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * This class will define the menu bar
 * 
 * @author Hayden Smith
 * 
 * @version 6/28/17
 *
 */
public class NimMenuBar extends BorderPane {
	private Game theGame;
	private HumanPane pnHumanPlayer;
	private ComputerPane pnComputerPlayer;
	private StatusPane pnGameInfo;
	
	private NimHelpDialog help;
	private boolean value;
	private NewGamePane pnChooseFirstPlayer;
	
	/**
	 * Constructor that accepts a game object
	 * 
	 * @param theGame		theGame
	 * @param newGame		newGame
	 * @param value			value
	 * 
	 * Precondition:		theGame != null
	 * 						newGame != null
	 * 						value != null
	 * 
	 * Postcondition:		object will be initialized to passed values
	 */
	public NimMenuBar(Game theGame, NewGamePane newGame, boolean value) {
		if (theGame == null || newGame == null) {
			throw new IllegalArgumentException("Menu could not be created");
		} 
		
		this.help = new NimHelpDialog();
		this.theGame = theGame;
		this.pnChooseFirstPlayer = newGame;
		this.pnGameInfo = this.pnChooseFirstPlayer.theStatus();
		this.pnHumanPlayer = this.pnChooseFirstPlayer.getHuman();
		this.pnComputerPlayer = this.pnChooseFirstPlayer.getComputer();
		this.value = value;
		
		this.createMenu();
	}
	
	/**
	 * This method will create the menu bar for the game
	 * 
	 * @param 			theGame
	 * 
	 * Precondition: 	theGame != null
	 * 
	 * Postcondition:	the vbox will be returned
	 * 
	 * @return 			the menubar vbox
	 */
	private void createMenu() {
		
		VBox vbxMenuHolder = new VBox();
		MenuBar mnuMain = new MenuBar();
		
		Menu mnuFile = this.createFileMenu();
		
		Menu mnuSettings = this.createStrategyMenu();
		
		Menu mnuHelp = this.createHelpMenu();
				
		mnuMain.getMenus().addAll(mnuFile, mnuSettings, mnuHelp);
		vbxMenuHolder.getChildren().addAll(mnuMain);
		this.setTop(vbxMenuHolder);
	}
	
	private Menu createFileMenu() {
		Menu mnuFile = new Menu("_File");
		mnuFile.setMnemonicParsing(true);
	
		MenuItem mnuNew = new MenuItem("_New");
		mnuNew.setMnemonicParsing(true);
		mnuNew.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
		mnuNew.setOnAction(new NewGame());
		
		MenuItem mnuExit = new MenuItem("E_xit");
		mnuExit.setMnemonicParsing(true);
		mnuExit.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN));
		mnuExit.setOnAction(event -> System.exit(0));
		
		mnuFile.getItems().addAll(mnuNew, mnuExit);
		return mnuFile;
	}
	
	private Menu createStrategyMenu() {
		Menu mnuSettings = new Menu("_Strategy");
		mnuSettings.setMnemonicParsing(true);
		
		ToggleGroup tglStrategy = new ToggleGroup();
		
		RadioMenuItem cautious = this.addStrategyItem(new CautiousStrategy(), KeyCode.C, "_Cautious", tglStrategy);
		RadioMenuItem greedy = this.addStrategyItem(new GreedyStrategy(), KeyCode.G, "_Greedy", tglStrategy);
		RadioMenuItem random = this.addStrategyItem(new RandomStrategy(), KeyCode.R, "_Random", tglStrategy);
		RadioMenuItem expert = this.addStrategyItem(new ExpertStrategy(), KeyCode.E, "_Expert", tglStrategy);
		
		NumberOfSticksStrategy currentStrategy = this.theGame.getComputerPlayer().getStrategy();
		if (currentStrategy.getClass() == CautiousStrategy.class) {
			cautious.setSelected(true);
		} else if (currentStrategy.getClass() == RandomStrategy.class) {
			random.setSelected(true);
		} else if (currentStrategy.getClass() == GreedyStrategy.class) {
			greedy.setSelected(true);
		} else {
			expert.setSelected(true);
		}

		mnuSettings.getItems().addAll(cautious, greedy, random, expert);
		return mnuSettings;
	}
	
	/*
	 * Private helper to create radio buttons corresponding to each strategy and set them in the toggle group
	 */
	private RadioMenuItem addStrategyItem(NumberOfSticksStrategy strategy, KeyCode code, String item, ToggleGroup group) {
		RadioMenuItem menuItem = new RadioMenuItem(item);
		
		menuItem.setAccelerator(new KeyCodeCombination(code, KeyCombination.SHORTCUT_DOWN));
		menuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				NimMenuBar.this.theGame.getComputerPlayer().setStrategy(strategy);
			}
		});
		menuItem.setMnemonicParsing(true);
		menuItem.setToggleGroup(group);
		
		return menuItem;
	}
	
	/*
	 * This will create the help menu and add two menu items using other private methods 
	 */
	private Menu createHelpMenu() {
		Menu mnuHelp = new Menu("_Help");
		mnuHelp.setMnemonicParsing(true);
		
		MenuItem help = this.helpItems("H_elp", KeyCode.P);
		MenuItem about = this.helpItems("_About", KeyCode.A);
		
		help.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				NimMenuBar.this.showHelp();
			}
		});
		
		about.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				NimMenuBar.this.showAbout();
			}
		});
		
		mnuHelp.getItems().addAll(help, about);
		return mnuHelp;
	}
	
	/*
	 * Private helper to create menu items for Help menu
	 */
	private MenuItem helpItems(String item, KeyCode code) {
		MenuItem theItem = new MenuItem(item);
		theItem.setMnemonicParsing(true);
		theItem.setAccelerator(new KeyCodeCombination(code, KeyCombination.SHORTCUT_DOWN));
		return theItem;
	}
	
	/*
	 * Method to call rules menu
	 */
	private void showHelp() {
		this.value = this.help.showHelpDialog();
	}
	
	/*
	 * Method to call about menu
	 */
	private void showAbout() {
		Alert message = new Alert(AlertType.INFORMATION);
		message.setTitle("CS6910 - Better Nim");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		Date date = new Date();
		
		String theDate = dateFormat.format(date);
		
		String helpMessage = "Author: Hayden Smith\n"
				+ "\nCurrent Date: " + theDate;
		
		message.setHeaderText(helpMessage);
		message.setContentText("Press 'Ok' to return to game");
		
		message.showAndWait();
	}
	
	/**
	 * Private inner class to handle the launching of new game
	 * 
	 * @author Hayden Smith
	 * 
	 * @version 6/28/17
	 *
	 */
	private class NewGame implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			NimMenuBar.this.pnChooseFirstPlayer.reset();
			NimMenuBar.this.pnChooseFirstPlayer.setDisable(false);
			NimMenuBar.this.pnHumanPlayer.setDisable(true);
			NimMenuBar.this.pnComputerPlayer.setDisable(true);
			NimMenuBar.this.pnComputerPlayer.resetNumberTaken();
			NimMenuBar.this.pnHumanPlayer.resetNumberToTakeComboBox();
			NimMenuBar.this.pnGameInfo.update();
			
			if (NimMenuBar.this.value) {
				NimMenuBar.this.value = NimMenuBar.this.help.showHelpDialog();
			}
		}
		
	}
	
}