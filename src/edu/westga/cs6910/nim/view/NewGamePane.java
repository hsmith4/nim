package edu.westga.cs6910.nim.view;

import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.Player;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;

/**
 * This class will define the new game pane
 * 
 * @author Hayden Smith
 * 
 * @version 6/29/17
 *
 */
public class NewGamePane extends GridPane {
	private RadioButton radHumanPlayer;
	private RadioButton radComputerPlayer;
	private RadioButton radRandomPlayer;
	private ComboBox<Integer> cmbInitialSize;
	
	private Game theGame;
	private Player theHuman;
	private Player theComputer;
	private StatusPane status;
	private ComputerPane pnComputerPlayer;
	private HumanPane pnHumanPlayer;
	
	/**
	 * Constructor that accepts a game object
	 * 
	 * @param theGame		theGame
	 * @param human			human
	 * @param computer		computer
	 * 
	 * Precondition:		theGame != null
	 * 						human != null
	 * 						computer != null
	 * 
	 * Postcondition:		NewGamePane will be initialized with passed parameter
	 */
	public NewGamePane(Game theGame, HumanPane human, ComputerPane computer) {		
		if (theGame == null) {
			throw new IllegalArgumentException("Game cannot be null");
		} 
		
		if (human == null || computer == null) {
			throw new IllegalArgumentException("One or more panes not found");
		}
		
		this.theGame = theGame;
		
		this.pnComputerPlayer = computer;
		this.pnHumanPlayer = human;
		this.status = new StatusPane(theGame);
		this.theHuman = this.theGame.getHumanPlayer();
		this.theComputer = this.theGame.getComputerPlayer();
		
		this.buildPane();
	}
	
	/**
	 * This method will return the current StatusPane
	 * 
	 * @return		status
	 */
	public StatusPane theStatus() {
		return this.status;
	}
	
	private void buildPane() {
		this.setHgap(20);
		
		this.createInitialPileSizeItems();
		
		this.createFirstPlayerItems();	
	}
	
	/**
	 * This method will return the current computer pane
	 * 
	 * @return		pnComputerPlayer
	 */
	public ComputerPane getComputer() {
		return this.pnComputerPlayer;
	}
	
	/**
	 * This method will return the current human pane
	 * 
	 * @return		pnHumanPlayer
	 */
	public HumanPane getHuman() {
		return this.pnHumanPlayer;
	}

	private void createFirstPlayerItems() {
		Label lblFirstPlayer = new Label("Who plays first? ");
		this.add(lblFirstPlayer, 2, 0);
		
		this.radHumanPlayer = new RadioButton(this.theHuman.getName() + " first");	
		this.radHumanPlayer.setOnAction(new HumanFirstListener());

		this.radComputerPlayer = new RadioButton(this.theComputer.getName() + " first");
		this.radComputerPlayer.setOnAction(new ComputerFirstListener());
		
		this.radRandomPlayer = new RadioButton("Random Player");
		this.radRandomPlayer.setOnAction(new RandomFirstListener());

		ToggleGroup group = new ToggleGroup();
		this.radHumanPlayer.setToggleGroup(group);
		this.radComputerPlayer.setToggleGroup(group);
		this.radRandomPlayer.setToggleGroup(group);
		
		this.add(this.radHumanPlayer, 3, 0);
		this.add(this.radComputerPlayer, 4, 0);
		this.add(this.radRandomPlayer, 5, 0);
	}

	private void createInitialPileSizeItems() {
		Label lblInitialPileSize = new Label("Initial Pile Size: ");
		this.add(lblInitialPileSize, 0, 0);
		
		this.cmbInitialSize = new ComboBox<Integer>();
		this.cmbInitialSize.getItems().addAll(9, 16, 21);
		this.cmbInitialSize.setValue(9);
		this.cmbInitialSize.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				int pileSize = NewGamePane.this.cmbInitialSize.getValue();
				NewGamePane.this.theGame.setPileSize(pileSize);
				NewGamePane.this.status.update();
			}
		});
		this.add(this.cmbInitialSize, 1, 0);
	}

	/**
	 * Resets the radio buttons for new selection
	 */
	public void reset() {
		NewGamePane.this.theGame.setPileSize(NewGamePane.this.cmbInitialSize.getValue());
		this.radHumanPlayer.setSelected(false);
		this.radComputerPlayer.setSelected(false);
		this.radRandomPlayer.setSelected(false);
	}
	
	/* 
	 * Defines the listener for computer player first button.
	 */		
	private class RandomFirstListener implements EventHandler<ActionEvent> {
		@Override
		/** 
		 * Enables the ComputerPlayerPanel and starts a new game. 
		 * Event handler for a click in the computerPlayerButton.
		 */
		public void handle(ActionEvent arg0) {
			int initialPileSize = NewGamePane.this.cmbInitialSize.getValue();
			
			NewGamePane.this.setDisable(true);

			if (Math.random() * 10 <= 4) {
				NewGamePane.this.pnComputerPlayer.setDisable(false);
				NewGamePane.this.theGame.startNewGame(NewGamePane.this.theComputer, initialPileSize);					
			} else {
				NewGamePane.this.pnHumanPlayer.setDisable(false);
				NewGamePane.this.theGame.startNewGame(NewGamePane.this.theHuman, initialPileSize);
			}
		}
	}
	
	/* 
	 * Defines the listener for human player first button.
	 */	
	private class HumanFirstListener implements EventHandler<ActionEvent> {
		/* 
		 * Sets up user interface and starts a new game. 
		 * Event handler for a click in the human player button.
		 */
		@Override
		public void handle(ActionEvent event) {
			int initialPileSize = NewGamePane.this.cmbInitialSize.getValue();
			NewGamePane.this.setDisable(true);
			NewGamePane.this.pnHumanPlayer.setDisable(false);
			NewGamePane.this.theGame.startNewGame(NewGamePane.this.theHuman, initialPileSize);
		}
	}
	
	/* 
	 * Defines the listener for computer player first button.
	 */		
	private class ComputerFirstListener implements EventHandler<ActionEvent> {
		@Override
		/** 
		 * Enables the ComputerPlayerPanel and starts a new game. 
		 * Event handler for a click in the computerPlayerButton.
		 */
		public void handle(ActionEvent arg0) {
			int initialPileSize = NewGamePane.this.cmbInitialSize.getValue();
			
			NewGamePane.this.pnComputerPlayer.setDisable(false);
			NewGamePane.this.setDisable(true);
			NewGamePane.this.theGame.startNewGame(NewGamePane.this.theComputer, initialPileSize);
		}
	}
	
}
