package edu.westga.cs6910.nim.view;

import edu.westga.cs6910.nim.model.Game;
import javafx.scene.layout.BorderPane;


/**
 * Defines a GUI for the 1-pile Nim game.
 * This class was started by CS6910
 * 
 * @author modified by Hayden Smith
 * 
 * @version 6/28/17
 * 
 */
public class NimPane extends BorderPane {
	private FullNimPane thePane;
	private NimMenuBar theMenu;
	private NimHelpDialog help;
	private Boolean shouldShow;
	
	/**
	 * Creates a pane object to provide the view for the specified
	 * Game model object.
	 * 
	 * @param theGame	the domain model object representing the Nim game
	 * 
	 * @requires theGame != null
	 * @ensures	 the pane is displayed properly
	 */
	public NimPane(Game theGame) {
		if (theGame == null) {
			throw new IllegalArgumentException("Invalid game");
		}
		
		this.help = new NimHelpDialog(); 
		this.shouldShow = this.help.showHelpDialog();
		
		this.thePane = new FullNimPane(theGame);
		this.theMenu = new NimMenuBar(theGame, this.thePane.getFirstPlayer(), this.shouldShow);
		
		this.setTop(this.theMenu);
		this.setCenter(this.thePane);
	}
}
