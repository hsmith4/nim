package edu.westga.cs6910.nim.view;

import edu.westga.cs6910.nim.model.Game;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * This class will create the entire pane 
 * 
 * @author Hayden Smith
 * 
 * @version 6/29/17
 *
 */
public class FullNimPane extends BorderPane {
	private Game theGame;
	private BorderPane pnContent;
	private HumanPane pnHumanPlayer;
	private ComputerPane pnComputerPlayer;
	private StatusPane pnGameInfo;
	private NewGamePane pnChooseFirstPlayer;
	
	/**
	 * Creates a pane object to provide the view for the specified
	 * Game model object.
	 * 
	 * @param theGame	the domain model object representing the Nim game
	 * 
	 * @requires theGame != null
	 * @ensures	 the pane is displayed properly
	 */
	public FullNimPane(Game theGame) {
		if (theGame == null) {
			throw new IllegalArgumentException("Invalid game");
		}
		this.theGame = theGame;
		this.pnContent = new BorderPane();
		
		this.buildPane();
	}

	private HBox createHBoxHolder(Pane newPane, boolean disable) {
		newPane.setDisable(disable);
		HBox leftBox = new HBox();
		leftBox.getStyleClass().add("pane-border");	
		leftBox.getChildren().add(newPane);
		return leftBox;
	}
	
	private void buildPane() {
		this.pnHumanPlayer = new HumanPane(this.theGame);		
		HBox leftBox = this.createHBoxHolder(this.pnHumanPlayer, true);
		this.pnContent.setLeft(leftBox);	

		this.pnComputerPlayer = new ComputerPane(this.theGame);
		HBox centerBox = this.createHBoxHolder(this.pnComputerPlayer, true);
		this.pnContent.setCenter(centerBox);
		
		this.pnChooseFirstPlayer = new NewGamePane(this.theGame, this.pnHumanPlayer, this.pnComputerPlayer);	
		HBox topBox = this.createHBoxHolder(this.pnChooseFirstPlayer, false);
		this.pnContent.setTop(topBox);	
		
		this.pnGameInfo = this.pnChooseFirstPlayer.theStatus();
		HBox bottomBox = this.createHBoxHolder(this.pnGameInfo, false);
		this.pnContent.setBottom(bottomBox);

		this.setCenter(this.pnContent);
	}
	
	/**
	 * This method will return the current first player chooser pane
	 * 
	 * @return		pnChooseFirstPlayer
	 */
	public NewGamePane getFirstPlayer() {
		return this.pnChooseFirstPlayer;
	}
}
