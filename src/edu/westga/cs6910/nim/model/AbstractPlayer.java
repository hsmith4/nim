package edu.westga.cs6910.nim.model;

/**
 * Models a generic player in the game Nim.
 * 
 * @author CS 6910
 * @version Summer 2017
 */
public abstract class AbstractPlayer implements Player {

	private String theName;
	private int sticksToTake;
	private Pile thePile;

	/**
	 * Creates a new AbstractPlayer with the specified name.
	 * 
	 * @requires: aName != null
	 * @ensures name().equals(aName)
	 * 
	 * @param aName	The player's name
	 */
	public AbstractPlayer(String aName) {
		if (aName == null) {
			throw new IllegalArgumentException("Null name");
		}
		this.theName = aName;
	}

	@Override
	/**
	 * @see Player#name()
	 */
	public String getName() {
		return this.theName;
	}

	@Override
	/**
	 * @see Player#sticksOnThisTurn()
	 */
	public int getSticksOnThisTurn() {
		return this.sticksToTake;
	}

	@Override
	/**
	 * @see Player#pileForThisTurn()
	 */
	public Pile getPileForThisTurn() {
		return this.thePile;
	}
	
	/**
	 * Sets the player's name
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Null name");
		}
		this.theName = name;
	}
	
	/**
	 * Implements Player's setNumberSticksToTake(int), but is not
	 * normally called by clients objects. Instead, clients usually
	 * call the 0-parameter version.
	 * 
	 * @see Player#setNumberSticksToTake(int)
	 */
	@Override
	public void setNumberSticksToTake(int number) {
		if (number < 1 || number > this.thePile.getSticksLeft()) {
			throw new IllegalArgumentException("Invalid number of sticks");
		}
		this.sticksToTake = number;
	}

	/**
	 * Sets the number of sticks to be taken on this turn
	 * @see Player#setNumberSticksToTake()
	 */
	public abstract void setNumberSticksToTake();

	@Override
	/**
	 * @see Player#setPileForThisTurn(Pile)
	 */
	public void setPileForThisTurn(Pile aPile) {
		if (aPile == null) {
			throw new IllegalArgumentException("Invalid pile");
		}
		this.thePile = aPile;
	}
	
	@Override
	/**
	 * @see Player#takeTurn(Pile) 
	 */	
	public void takeTurn() {				
		this.thePile.removeSticks(this.getSticksOnThisTurn());
	}
}