package edu.westga.cs6910.nim.model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * A pile of sticks for playing Nim.
 * @author CS 6910
 * @version Summer 2017
 */
public class Pile implements Observable {
	private IntegerProperty sticks;

	/**
	 * Creates a new Pile with the specified number of sticks.
	 * @param	sticks	The number of sticks in the Pile
	 * 
	 * @require sticks > 0
	 * @ensure sticksLeft() == sticks
	 */
	public Pile(int sticks) {
		if (sticks < 1) {
			throw new IllegalArgumentException("Invalid number of sticks");
		}
		this.sticks = new SimpleIntegerProperty(sticks);
	}

	/**
	 * Returns the number of sticks remaining in this Pile.
	 * 
	 * @return the size of this Pile
	 */
	public int getSticksLeft() {
		return this.sticks.getValue();
	}

	/**
	 * Reduces the number of sticks by the specified amount.
	 * @param	number	The number of sticks to be removed
	 * 
	 * @require number > 0 && number <= this.sticksLeft()
	 * @ensure this.sticksLeft() == sticksLeft() - number
	 */
	public void removeSticks(int number) {
		if (number > this.sticks.getValue() || number <= 0) {
			throw new IllegalArgumentException("Invalid number of sticks");
		}
		int newNumber = this.sticks.getValue() - number;
		this.sticks.setValue(newNumber);
	}

	/**
	 * Returns a String representation of this Pile.
	 * 
	 * @return "Pile size: n", where n is the number of sticks in this Pile
	 */
	public String toString() {
		return "Pile size: " + this.sticks.getValue();
	}

	@Override
	public void addListener(InvalidationListener listener) {
		this.sticks.addListener(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		this.sticks.removeListener(listener);
	}
}
