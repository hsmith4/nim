package edu.westga.cs6910.nim.model;

import edu.westga.cs6910.nim.model.strategy.NumberOfSticksStrategy;

/**
 * ComputerPlayer represents a very simple automated player in the game Nim.
 * It removes 1 stick at a time.
 * 
 * @author CS 6910
 * @version Summer 2017
 */
public class ComputerPlayer extends AbstractPlayer {
	private static final String NAME = "Better computer";
	private NumberOfSticksStrategy theStrategy;
	
	/**
	 * Creates a new ComputerPlayer with the specified strategy.
	 * 
	 * @param	theStrategy	The number of sticks strategy to be used
	 * 
	 * @requires theStrategy != null
	 * @ensures: the specified strategy will determine how many sticks to take
	 * 
	 * @ensure this.name().equals(name)
	 */
	public ComputerPlayer(NumberOfSticksStrategy theStrategy) {
		super(NAME);
		if (theStrategy == null) {
			throw new IllegalArgumentException("Invalid strategy");
		}
		this.theStrategy = theStrategy;
	}

	/**
	 * Implements Player's setNumberSticksToTake() to set the number
	 * of sticks to 1.
	 * 
	 * @ensure  sticksOnThisTurn() >= 1 && 
	 * 			sticksOnThisTurn() <= Math.min(pileForThisTurn.sticksLeft()-1, 
	 * 							 			   Game.MAX_STICKS_PER_TURN)
	 * 
	 * @see Player#setNumberSticksToTake()
	 */
	@Override
	public void setNumberSticksToTake() {
		super.setNumberSticksToTake(
				this.theStrategy.howManySticks(
						super.getPileForThisTurn().getSticksLeft()));
	}

	/**
	 * Sets this ComputerPlayer�s strategy to the specified value.
	 * 
	 * @requires theStrategy != null
	 * @ensures: the specified strategy will determine how many sticks to take
	 * @param theStrategy	the strategy to use
	 */
	public void setStrategy(NumberOfSticksStrategy theStrategy) {
		if (theStrategy == null) {
			throw new IllegalArgumentException("Invalid strategy");
		}
		this.theStrategy = theStrategy;
	}
		
	/**
	 * Returns the NumberOfSticksStrategy used by this player
	 * 
	 * @return	The NumberOfSticksStrategy being used
	 */
	public NumberOfSticksStrategy getStrategy() {
		return this.theStrategy;
	}
}