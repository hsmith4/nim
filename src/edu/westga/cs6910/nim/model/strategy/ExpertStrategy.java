package edu.westga.cs6910.nim.model.strategy;

/**
 * This class will create and define the expert strategy
 * 
 * @author Hayden Smith
 * 
 * @version 7/6/17
 *
 */
public class ExpertStrategy implements NumberOfSticksStrategy {
	
	/** 
	 * Implements NumberOfSticksStrategy#howManySticks(int) to return the most ideal number of sticks taken based
	 * on the pile size
	 * 
	 * @see edu.westga.nim.model.strategies.NumberOfSticksStrategy#howManySticks(int)
	 * @precondition pileSize > 0
	 * 
	 * @param pileSize the number of sticks on the pile
	 * 
	 * @return sticks taken
	 */
	@Override
	public int howManySticks(int pileSize) {
		if (pileSize % 4 == 0) {
			return 3;
		} else if (pileSize % 4 == 3) {
			return 2;
		} 
		
		return 1;
	}
}
